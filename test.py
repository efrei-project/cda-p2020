import sys
import math

def string2bits(s=''):
    return bin(int.from_bytes(s.encode(), 'big'))

message = "C"
message = message.upper()
binary = bin(int.from_bytes(message.encode(), 'big'))
binaryString = binary[2:]

answer = ""

suffix = ""
prefix = ""


for char in binaryString:
    if (char == "1" and prefix != "0") or (char == "0" and prefix != "00")
        answer += "{} {}".format(prefix, suffix)
        prefix = ""
        suffix = ""

    if char == "1":
        prefix = "0"
    else:
        prefix = "00"

    sufffix += '0'

print(answer)
