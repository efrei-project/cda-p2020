class mBinaryNode
{
  constructor(data)
  {
    this._data = data;
    this._left = null;
    this._right = null;
  }

  getData()
  {
    return this._data;
  }
  setData(value)
  {
    this._data= value;
  }
  getLeft()
  {
    return this._left;
  }
  setLeft(node)
  {
    this._left = node;
  }
  getRight()
  {
    return this._right;
  }
  setRight(node)
  {
    this._right = node;
  }
}
module.exports = mBinaryNode;
