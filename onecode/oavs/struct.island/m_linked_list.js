const mNode = require("./m_node.js");

class mLinkedList
{
  constructor()
  {
    this._head = null;
    this.lastNode = null;
    this._length = 0;
  }

  length()
  {
    return this._length;
  }

  isEmpty()
  {
    if(this._head)
    {
      return false;
    }
    else
    {
      return true;
    }
  }

  push(data)
  {
    let toReturn;
    if(Array.isArray(data))
    {
      toReturn = this.pushList(data);
    }
    else
    {
      toReturn = this.pushItem(data);
    }
  }

  pushList(dataList)
  {
    let nodeList = [];
    for(var i = 0; i < dataList.length; i++)
    {
      let data = dataList[i];
      let mnode = this.pushItem(data);
      nodeList.push(mnode);
    }
    return nodeList;
  }
  pushItem(data)
  {
    let mnode = new mNode(data);
    if(!this._head)
    {
      this._head = mnode;
      this.lastNode = mnode;
      this._length++;
      return this.lastNode
    }
    else
    {
      this.lastNode.setNext(mnode);
      this.lastNode = mnode;
      this._length++;
      return this.lastNode;
    }
  }

  pop()
  {
    let ancientFirst = this._head;
    let newFirst = this._head.getNext();
    this._head.setNext(null);
    this._head = newFirst;
    this._length--
    return this.toArray();
  }

  getNodeByIndex(index)
  {
    if((index == 0)| (index > this._length) | (index < 0))
    {
      return null;
    }

    let cursor = this._head;
    for(var i = 1; i <= index;i++)
    {
      if(i == index)
      {
        return cursor;
      }
      else
      {
        cursor = cursor.getNext();
      }
    }
  }

  popNodeByIndex(index)
  {
    let previous = this.getNodeByIndex(index -1);
    let next =  this.getNodeByIndex(index +1);
    let actual = this.getNodeByIndex(index);
    previous ? previous.setNext(next) : this._head = next;
    next ? actual.setNext(null) : this.lastNode = previous;
    actual.setNext(null);
    return actual;
  }

  getNodeByValue(value)
  {
    let cursor = this._head;
    while(true)
    {
      if(cursor.getData() === value)
      {
        return cursor;
      }
      else
      {
        cursor = cursor.getNext();
      }
    }
  }

  reverseList()
  {
    let nodeArray = this.toArray();
    let dataArray = [];
    var tmp = nodeArray.forEach(function(element)
                    {
                      dataArray.push(element.getData());
                    });
    let reversed = dataArray.reverse();
    while(this._head)
    {
      this.pop();
    }
    return this.push(reversed);
  }

  forEach(func)
  {
      if(typeof func === "function")
      {
        let node = this._head;
        while(node)
        {
          func(node);
          node = node.getNext();
        }
      }
  }

  toArray()
  {
    let cursor = this._head;
    let arrayToReturn = [];
    for (var i = 0; i <= this._length -1; i++)
    {
      arrayToReturn.push(cursor);
        cursor = cursor.getNext()
    }
    return arrayToReturn;
  }


  toString()
  {
    let myString = "";
    var node = this._head;
    while (node)
    {
      myString += this.toStringFromNode(node);
      node = node.getNext();
    }
    return myString;
  }


  toStringFromNode(node)
  {
    let data = node.getData();
    let next;
    node.getNext() ? next = node.getNext().getData() : next = null;
    let myString =  "Node avec une Data de (" + data + ") et son Next est (" + next + ").\n";
    return myString;
  }



}
module.exports = mLinkedList;
