const mLinkedList = require("./m_linked_list.js");
class mStack
{
  constructor()
  {
    this._list = new mLinkedList;
  }

  size()
  {
    return this._list.length();
  }

  isEmpty()
  {
    return this._list.isEmpty();
  }
  push(value)
  {
    this._list.push(value);
  }
  pop()
  {
    this._list.popNodeByIndex(this.size());
  }
  top()
  {
    this._list.getNodeByIndex(this.size());
  }


}
module.exports = mStack;
