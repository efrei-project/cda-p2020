const mBinaryNode = require("./m_binary_node.js");
class mTree
{
  constructor()
  {
    this._root = null;
  }

  getRoot()
  {
    return this._root;
  }

  search(data, cursor = this.getRoot())
  {
    if (!cursor)
    {
      console.log("The tree does not grow up yet !");
      return false;
    }
    let interrupt = true;
    while(interrupt)
    {
      if(data == cursor.getData())
      {
        return cursor;
      }
      if(data < cursor.getData())
      {
        if(cursor.getLeft())
        {
          cursor = cursor.getLeft();
        }
        else
        {
          console.log("inférieur et rien à gauche");
          interrupt = false;
        }
      }
      else if(data > cursor.getData())
      {
        if(cursor.getRight())
        {
          cursor = cursor.getRight();
        }
        else
        {
          console.log("supérieur et rien à droite");
          interrupt = false;
        }
      }
    }
  }

  insert(data)
  {
    let node = new mBinaryNode(data);
    if(!this.getRoot())
    {
      this._root = node;
      return true;
    }
    let cursor = this.getRoot();
    let tmp = true;
    while(tmp)
    {
      if(data < cursor.getData())
      {
        if(!cursor.getLeft())
        {
          cursor.setLeft(node);
          tmp = false;
        }
        else
        {
          cursor = cursor.getLeft();
        }
      }
      else
      {
        if(!cursor.getRight())
        {
          cursor.setRight(node);
          tmp = false;
        }
        else
        {
          cursor = cursor.getRight();
        }
      }
    }
  }

  remove(data, cursor = this._root)
  {
    if (!cursor)
    {
      console.log("The tree does not grow up yet !");
      return false;
    }
    if(data < cursor.getData())
    {
      data == cursor.getLeft().getData() ? cursor.setLeft(null) : cursor = cursor.getLeft();
    }
    else
    {
      data == cursor.getRight().getData() ? cursor.setRight(null) : cursor = cursor.getRight();
    }
  }

  inorder(node = this._root)
  {
    if(node)
    {
      this.inorder(node.getLeft());
      console.log(node.getData());
      this.inorder(node.getRight());
    }
  }

  deep()
  {
    let int = 5;
    return int;
  }

}
module.exports = mTree;
