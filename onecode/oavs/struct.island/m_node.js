class mNode
{
  constructor(data)
  {
    this._data = data;
    this._next = null;
  }

  getData()
  {
    return this._data;
  }

  setData(data)
  {
    this._data = data;
  }

  getNext()
  {
    return this._next;
  }

  setNext(mnode)
  {
    this._next = mnode;
  }

}
module.exports = mNode;
