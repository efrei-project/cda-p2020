class mSort
{
  constructor()
  {

  }

  static selection(list)
  {
    let n = list.length
    for(var i = 0; i < n; i++)
    {
      for(var j = 1; j < n; j++)
      {
        if(list[j] < list[j - 1])
        {
          list = this.swap(list, j, j -1)
        }
      }
    }
    return list;
  }

  static bubble(list)
  {
    let n = list.length
    let swapped;
    do {
      swapped = false;
      for(var i = 0; i < n - 1; i++)
      {
        if(list[i] > list[i + 1])
        {
          list = this.swap(list, i, i+1)
          swapped = true;
        }
      }
    } while (swapped);
    return list;
  }

  static insertion(list)
  {
    let n = list.length
    for(var i = 1, value; i < n; i++)
    {
      value = list[i];
      for(var j = i; list[j - 1] > value; j--)
      {
        list[j] = list[j - 1];
      }
      list[j] = value;
    }
    return list;
  }

  static merge(list)
  {
    let n = list.length

    return list;
  }

  static quick(list)
  {
    return list;
  }

  static swap(list,firstIndex, lastindex)
  {
    let tmp = list[firstIndex];
    list[firstIndex] = list[lastindex];
    list[lastindex] = tmp;
    return list;
  }

}
module.exports = mSort;
