const mSort = require("./m_sort.js");
const arr = [1337, 42, 86, 7, 3, 9, 1859, 22, 10, 1921];
const bubble = [1337, 42, 86, 7, 3, 9, 1859, 22, 10, 1921];
const selection = [1337, 42, 86, 7, 3, 9, 1859, 22, 10, 1921];
const insertion = [1337, 42, 86, 7, 3, 9, 1859, 22, 10, 1921];
const merge = [1337, 42, 86, 7, 3, 9, 1859, 22, 10, 1921];
const quick = [1337, 42, 86, 7, 3, 9, 1859, 22, 10, 1921];
console.log("\n");
console.log("----------------");
console.log("Ma référence");
console.log(arr);

console.log("\n");
console.log("----------------");
console.log("Ma bubble");
console.log(mSort.bubble(bubble));

console.log("\n");
console.log("----------------");
console.log("Ma selection");
console.log(mSort.selection(selection));

console.log("\n");
console.log("----------------");
console.log("Ma insertion");
console.log(mSort.insertion(insertion));

console.log("\n");
console.log("----------------");
console.log("Ma merge");
console.log(mSort.merge(merge));

console.log("\n");
console.log("----------------");
console.log("Ma quick");
console.log(mSort.quick(quick));
