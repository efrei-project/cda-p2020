const e01 = require('./eventbox.js');
const e03 = require('./streambox.js');
/*
console.log('\n');
console.log('----------');
console.log("E01:");
e01.empty();
console.log('\n');
console.log('----------');
console.log("E01:");
e01.empty('Ch0o0o000oopppeer !!');

console.log('\n');
console.log('----------');
console.log("E01:");
let names = ['Luffy', 'Zoro', 'Usopp', 'Robin', 'Nami', 'Sanji', 'Ch0pper'];
e01.withArgs(names);
*/

e03.duplicate("Ch0pin.jpg");

e03.transform('Ch0pin.resume.md', /\w*/, element =>  element.toUpperCase());
/*

let temp = "coucou les enfants";
let re = /enfants/;
let truc = temp.replace(re, temp.toUpperCase());
console.log(truc);
*/
