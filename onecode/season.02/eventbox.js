const EventEmitter = require('events');

function empty(message = "Hello world !"){
  let event = new EventEmitter();
  event.on('empty', text => console.log(text));
  event.emit('empty', message);
}

function withArgs (names){
  names.forEach(name =>empty(`Here come's a new pirate ->> ${name}`));
}


module.exports ={
                  empty,
                  withArgs
                };
