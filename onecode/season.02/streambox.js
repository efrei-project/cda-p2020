const fs = require('fs');
const path  = require ('path');
const stream = require('stream');

function duplicate(filename){
  let read = fs.createReadStream('resources/' + filename);
  let write = fs.createWriteStream('resources/duplicated_' + filename);
  read.on('data', function(){});
  read.pipe(write);
  write.on('data', function(){});
}

function transform(filename, re, fn){

  let read = fs.createReadStream('./resources/' + filename);
  let writed = fs.createWriteStream('resources/duplicated_' + filename);
  let streamTransform = new stream.Transform();
  let transformed = ""
  streamTransform._transform = (chunk, enc, cb) => {
                                let string = chunk.toString();
                                for (letter of string){
                                  if (letter.match(re)){
                                    letter = fn(letter);
                                  }
                                  transformed += letter;
                                }
                                console.log(transformed);
                              }
  writed.
  let readPipe = read.pipe(streamTransform);
}


module.exports ={
                  duplicate,
                  transform
                }
