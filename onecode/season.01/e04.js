const fs = require('fs');

exports.myReadFile = (file, func) => fs.existsSync(file) ?
                              fs.readFile(file, 'utf8', (err,data) => err ? console.log(data) : func(data)) :
                              console.log("File doesn't exist !");
