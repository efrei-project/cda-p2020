const path = require ('path');
const fs = require('fs');
exports.returnExtension = file => fs.existsSync(file) ? console.log(path.extname(file)) : console.log("File doesn't exist !");
