const fs = require('fs');

exports.changeRight = function (file,rights)
{
  fs.chmodSync(file, parseInt(rights, 8));
}
