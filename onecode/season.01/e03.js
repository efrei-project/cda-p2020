const fs = require('fs');

exports.myReadFile = file => fs.existsSync("./" + file) ? console.log(fs.readFileSync(file, 'utf8')) : console.log("File (" + file + ") doesn't exist at ./" + file);
