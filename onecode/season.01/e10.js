const fs = require('fs');
const path = require('path');


exports.copyDirectory = function(origin, copy)
{
  if(!(fs.existsSync(copy)))
  {
    fs.mkdirSync(copy);
  }

//todo le stream
//todo les sub folder
  fs.readdir(origin, (err, files) =>
  {
    files.forEach(file =>
    {
      from = path.join(origin,file);
      to = path.join(copy,file);
      let stat = fs.lstatSync(from);
      if (stat.isFile())
      {
        fs.copyFile(from, to, (err) =>
        {
          if(err) throw err;
        })
      }
      else if (stat.isDirectory())
      {
        this.copyDirectory(from, to);
      }
    })
    console.log('Directory [ ' + origin + ' ] successfully duplicated into [ ' + copy + ' ].');
  })
}
