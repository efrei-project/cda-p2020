const fs = require('fs');
exports.myWriteFile = (name, text) =>
{
  fs.writeFile('./' + name, text, function(err)
                                  {
                                    if(err){
                                      return console.log(err);
                                    }
                                    console.log('File ' + name + ' successfully created!');
                                  })
}
