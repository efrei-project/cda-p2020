const fs = require('fs');

exports.myCanWriteRead = file =>
{
  fs.access(file, fs.constants.W_OK | fs.constants.R_OK, function(err)
  {
    err ? console.log('I don\'t have acces to the file ' + file) : console.log('I can read or write the file ' + file);
  }
}
