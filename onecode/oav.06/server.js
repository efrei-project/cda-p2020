const express = require("express");
const app = express();
const Sequelize = require('sequelize');
const session = require('express-session');
const sequelize = new Sequelize('database', 'trucmuche', 'sdkjhsdfjhsdf', {
  host: 'localhost',
  dialect: 'sqlite',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // SQLite only
  storage: 'database.sqlite',

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});
let userSession;
app.use(session({secret:'ssshhhhhh'}));

//-------------------------------------------------------------------------
//---------                                                 ---------------
//---------                Model                            ---------------
//---------                                                 ---------------
//-------------------------------------------------------------------------

const User = sequelize.define('users', {
    id: {
      type: Sequelize.UUID,
      primaryKey: true,
      defaultValue: Sequelize.UUIDV4
    },
    nickname: {
      type: Sequelize.STRING,
      required: true
    },
    email: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.STRING,
      required: true
    },
    fullname: Sequelize.STRING,
    required: true
    });



//-------------------------------------------------------------------------
//---------                                                 ---------------
//---------                CREATE                           ---------------
//---------                                                 ---------------
//-------------------------------------------------------------------------

function createUser(nickname, password, email, fullname){
  let tmp = User.sync().then(() => {
    User.create({
      nickname: nickname,
      email: email,
      password: password,
      fullname: fullname
    })
  });
  return tmp;
}
//-------------------------------------------------------------------------
//---------                                                 ---------------
//---------                READ ONE                         ---------------
//---------                                                 ---------------
//-------------------------------------------------------------------------
 function readUser(name){
  let temp = User.sync().then(() => {
    User.findOne({
      nickname: name
    })
  }).then(user => console.log(user));
  return temp;
};
//-------------------------------------------------------------------------
//---------                                                 ---------------
//---------                READ ALL                         ---------------
//---------                                                 ---------------
//-------------------------------------------------------------------------
async function readALLUser(){
  await User.sync()
  let data = await User.findAll({raw: true})
  return data;
};


//-------------------------------------------------------------------------
//---------                                                 ---------------
//---------                UPDATE                           ---------------
//---------                                                 ---------------
//-------------------------------------------------------------------------
async function updateUser(id, nickname, password, email, fullname){

  await User.sync();
  let user = await User.findById(id);
  console.log(user);
  await User.update({
                  nickname: nickname,
                  email: email,
                  password: password,
                  fullname: fullname
                },
                {
                  where: {
                    id: id
                  }
                });

  let truc = await User.findById(id);
  console.log(truc);
  return user;
}


//-------------------------------------------------------------------------
//---------                                                 ---------------
//---------                DELETE                           ---------------
//---------                                                 ---------------
//-------------------------------------------------------------------------
/*
console.log("--------                CREATE                           ---------------");
createUser("Slok","test","truc@machin.com","uaisouais")
createUser("Lea","test1","truc@machin.com","Lea test")
createUser("John","test2","truc@machin.com","John test")
createUser("Doe","test3","truc@machin.com","Doe test")
createUser("Pierre","test4","truc@machin.com","Pierre test")
createUser("Justine","test5","truc@machin.com","Justine test")
createUser("Leila","test6","truc@machin.com","Leila test")
*/


app.listen(3000);
app.set('view engine', 'ejs');

app.get('/', async function(req, res, next){
  res.render('pages/index', {
    page: '',
    data: ''
  })
});

app.get('/login', async function(req, res, next) {
  res.render('pages/index',{
    page: 'block_login',
    data: ''
  })
});

app.post('/login', async function(req, res, next) {
  nickname = req.body.nickname
  password = req.body.password
  let user = await readUser(nickname)
  let list = await readALLUser();
  user.password == password ? userSession = req.session : userSession = null;
  res.render('pages/index',{
    page: 'block_logged',
    data : list
  })
});

app.post('/register', async function (req, res, next){
  nickname = req.body.nickname;
  password = req.body.password;
  email = req.body.email;
  fullname = req.body.fullname;
  await createUser(nickname, password, email, fullname);
  res.render('pages/index', {
    page: '',
    data: ''
  })
})
app.get('/register', async function (req, res, next){
  res.render('pages/index',{
    page: 'block_register',
    data: ''
  })
})
