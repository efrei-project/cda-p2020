const http = require('http'),
      url = require('url'),
      fs = require('fs'),
      querystring = require('querystring');
let port = "";
process.argv[2] ? port = process.argv[2] : port = 3000;

function processPost(request, response, callback) {
    var queryData = "";
    if(typeof callback !== 'function') return null;

    if(request.method == 'POST') {
        request.on('data', function(data) {
            queryData += data;
            if(queryData.length > 1e6) {
                queryData = "";
                response.writeHead(413, {'Content-Type': 'text/plain'}).end();
                request.connection.destroy();
            }
        });

        request.on('end', function() {
            request.post = querystring.parse(queryData);
            callback();
        });

    } else {
        response.writeHead(405, {'Content-Type': 'text/plain'});
        response.end();
    }
}

http.createServer((request, response) =>
                                      {
                                        let parsedURL = url.parse(request.url, true);
                                        if (parsedURL.path.match(/messages/))
                                        {
                                          if (request.method === 'POST')
                                          {
                                            processPost(request, response, function()
                                                                          {
                                                                            postResponse(request.post);
                                                                          })
                                          }
                                          response.writeHead(200, "OK", {'Content-Type': 'text/html'});
                                          response.end(generateHTML());
                                        }
                                      }).listen(port, (err) =>
                                                          {
                                                            if(err)
                                                            {
                                                              return console.log('something bad happened', err);
                                                            }
                                                            console.log('Server is running on ' + port);
                                                          });


function postResponse(args)
{
  let montext = "", monauteur = "", toto =0;
  let date = new Date();
  for (let [key, value] of Object.entries(args))
  {
    key == 'comment' ? montext = value : toto = 0;
    key == "auteur" ? monauteur = value : toto = 0;
  }
  monauteur == "" ? monauteur = 'anonymous' : toto = 0;
  let newRow =
            {
              text: montext,
              auteur: monauteur,
              date: date
            };
  fs.readFile('./log.json', function (err,data)
                            {
                              let json = JSON.parse(data);
                              json.messages.push(newRow);
                              fs.writeFileSync('./log.json', JSON.stringify(json));
                            });

}

function afficheJSON()
{
  let tmp = fs.readFileSync('./log.json')
  let json = JSON.parse(tmp);
  return JSON.stringify(json);
}



function generateHTML()
{

  let toReturn = '<!DOCTYPE html><html><head><meta charset="utf-8"><title>Codeflix.guru</title></head><body><form method="post" action="/messages"><br><input type="text" name="auteur" >Votre nom<br><input type="text" name="comment" >Votre message<br><input type="submit" value="Submit"></form></div></div></div>';
  json = fs.readFileSync('./log.json');
  parse = JSON.parse(json);
  data = JSON.stringify(parse);

  for(var item of parse.messages)
  {
    let thisAuteur = item.auteur, thisDate = item.date, thisMessage = item.text;
    toReturn += '<p>Auteur : ' + thisAuteur + '</p><p>Date = ' + thisDate + '</p><p>Message = ' + thisMessage + '</p></br><p>--------------------------------------------</p></br>';
  }
  toReturn +='</body></html>';
  return toReturn;
}
