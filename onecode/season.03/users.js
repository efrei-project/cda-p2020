const fs = require('fs');
const jsonfile = require('jsonfile');

class User {
  constructor(login, password) {
    this.login = login;
    this.password = password;
    this.connected = false;
  }

  getLogin()
  {
    return this.login;
  }

  setLogin(login)
  {
    this.login = login;
  }

  getPassword()
  {
    return this.password;
  }

  setPassword(password)
  {
    this.password = password;
  }

  isConnected()
  {
    return this.connected;
  }

  setConnected(bool)
  {
    this.connected = bool;
  }


  serialize()
  {
    let data = require('./resources/account.json');
    let user = JSON.parse(data);
  }


}
module.exports = User;
