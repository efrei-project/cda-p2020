const http = require('http');
const fs = require('fs');
const index = fs.readFileSync('./resources/index.html');
let port = 0;
process.argv[2] ? port = process.argv[2] : port = 4242;

http.createServer((req, res) =>
                            {
                              res.writeHead(200,
                                {
                                  "Content-type": "text/html"
                                });
                              res.end(index);
                            }

).listen(port, (err) =>
                    {
                      if(err)
                      {
                        return console.log('something bad happened', err);
                      }
                      console.log('Server is running on ' + port);
                    });
