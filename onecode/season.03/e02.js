const http = require('http');
const port = process.argv[2];

http.createServer((req, res) =>
                            {
                              if (req.method === 'POST')
                              {
                                res.writeHead(200,
                                  {
                                    "Content-type": "text/html"
                                  });
                                res.end(postResponse());
                              }
                              else if (req.method === 'GET')
                              {
                                res.writeHead(200,
                                  {
                                    "Content-type": "text/html"
                                  });
                                res.end(getResponse());
                              }
                            }

).listen(port, (err) =>
                    {
                      if(err)
                      {
                        return console.log('something bad happened', err);
                      }
                      console.log('Server is running on ' + port);
                    });


function getResponse()
{
  return '<h1>Hello world !</h1>\n';
}
function postResponse()
{
  return 'Heisenberg\n';
}
