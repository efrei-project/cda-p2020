const http = require('http');
const port = process.argv[2];
let server = http.createServer(function(req, res)
                              {
                                res.writeHead(200,
                                  {
                                    "Content-type": "text/html"
                                  });
                                res.end('<h1>Hello world !</h1>');
                              });
server.listen(port, () => console.log(`Server is running at port ${port}`));
