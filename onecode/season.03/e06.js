const http = require('http');
const url = require('url');
const fs = require('fs');
let port = 0;
if (process.argv[2])
{
  port = process.argv[2];
}
else {
  port = 4242;
}


http.createServer((req, res) =>
                            {
                              res.end(getExtension(req, res));
                            }).listen(port, (err) =>
                                                  {
                                                    if(err)
                                                    {
                                                      return console.log('something bad happened', err);
                                                    }
                                                    console.log('Server is running on ' + port);
                                                  });

function getExtension(req, res)
{
  let mime = url.parse(req.url, true).query['mime'];
  let contenttype = "", path = "";

  switch (mime)
  {
  case 'mp3':
    res.writeHead(200,{"Content-type": "audio/mp3"});
    path = './resources/bb.mp3';
    break;

  case 'png':
    path = './resources/cb.png';
    break;

  case 'gif':
    path =  './resources/ch0pper.gif';
    break;

  case 'txt':
    res.writeHead(200,{"Content-type": "text/plain"});
    path = './resources/mike.txt'
    break;

  default:
    return "I woke up, I found the server without query paramters, that's all i know";
  }

  temp = fs.readFileSync(path);
  res.write(temp);
  res.end();



}
