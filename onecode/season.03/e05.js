const http = require('http');
const url = require('url');
let port = 0;
if (process.argv[2])
{
  port = process.argv[2];
}
else {
  port = 4242;
}


http.createServer((req, res) =>
                            {
                              res.writeHead(200,
                                {
                                  "Content-type": "text/html"
                                });
                              res.end(getHeader(req));
                            }).listen(port, (err) =>
                                                  {
                                                    if(err)
                                                    {
                                                      return console.log('something bad happened', err);
                                                    }
                                                    console.log('Server is running on ' + port);
                                                  });

function getHeader(req)
{

  let myString = "";
  myString += "Server is running at port " + port + "\n";
  myString += "My request header dump:\n";
  myString += "host : " + req.headers['host'] + "\n";
  myString += "user-agent : " + req.headers['user-agent'] + "\n";
  myString += "accept : " + req.headers['accept'];
  console.log(myString);
  return myString;

}
