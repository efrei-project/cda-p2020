class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    };
  }

  plus() {
    this.setState({ value: this.state.value + 1 });
    console.log("+", this.state.value);
  }

  minus() {
    this.setState({ value: this.state.value - 1 });
    console.log("-", this.state.value);
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <h1 className="title"> counter-component </h1>
          <hr />
          <div className="content">
            <p>
              {" "}
              the current value is >
              <strong> {this.state.value} </strong>
              <br />
              <br />
              <a className="button is-success" onClick={this.plus.bind(this)}>
                {" "}
                Plus{" "}
              </a>
              <span> &nbsp; </span>
              <a className="button is-danger" onClick={this.minus.bind(this)}>
                {" "}
                Minus{" "}
              </a>
            </p>
          </div>
        </div>
      </section>
    );
  }
}
ReactDOM.render(<Counter />, document.getElementById("root"));
