(function() {
  let rootDomElement;
  let rootGeneratedElement;

  function isFunction(func) {
    return typeof func === "function";
  }

  function isClass(cls) {
    if (isFunction(cls)) {
      const funcStr = Function.prototype.toString.call(cls);
      return /^class/.test(funcStr) || /_classCallCheck/.test(funcStr);
    }
    return false;
  }

  class Component {
    constructor(props) {
      this.props = props;
    }

    reRender() {
      while (rootDomElement.hasChildNodes()) {
        rootDomElement.removeChild(rootDomElement.lastChild);
      }
      ReactDOM.render(rootGeneratedElement, rootDomElement);
    }

    setState(obj) {
      this.state = Object.assign({}, this.state, obj);
      this.reRender();
    }
  }

  class React {
    static createElement(element, props, ...children) {
      if (isClass(element)) {
        const component = new element(props);
        component.type = "MY_REACT_CLASS";
        return component;
      }
      if (isFunction(element)) {
        return element(props);
      }
      //create simple html node
      const myElement = document.createElement(element);

      // adding attributes
      for (let key in props) {
        if (/^on/.test(key)) {
          const type = key.substring(2).toLowerCase();
          myElement.addEventListener(type, props[key]);
        } else {
          const keyName = key == "className" ? "class" : key;
          myElement.setAttribute(keyName, props[key]);
        }
      }
      children.forEach(item => {
        if (item == null) {
          return;
        }
        if (typeof item === "object") {
          myElement.appendChild(item);
        } else {
          myElement.innerHTML += item;
        }
      });

      return myElement;
    }
  }

  class ReactDOM {
    static render(el, domElement) {
      rootGeneratedElement = el;
      rootDomElement = domElement;

      let domToDisplay;
      if (rootGeneratedElement.type == "MY_REACT_CLASS") {
        domToDisplay = rootGeneratedElement.render();
      } else {
        domToDisplay = rootGeneratedElement;
      }
      rootDomElement.appendChild(domToDisplay);
    }
  }

  window.React = React;
  window.ReactDOM = ReactDOM;
  window.React.Component = Component;
})();
