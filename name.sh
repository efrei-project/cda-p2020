#!/bin/bash

echo "Please enter the project name:"
read MODULE
echo "How many people ? [integer]"
read NUMBER

PROJECT_PATH=$HOME/codeflix/onecode/oavs/$MODULE

mkdir -p $PROJECT_PATH
echo $MODULE > $PROJECT_PATH/.oav.name
echo $FULLNAME > $PROJECT_PATH/.crew

if [ $NUMBER -gt 1 ] ; then
  for i in {1..$NUMBER} ; do
    echo "Please type the next firstname and lastname."
    read NAME
    echo $NAME >> $PROJECT_PATH/.crew
  done
fi


echo 'Module >>' $MODULE '<< successfully created!'
