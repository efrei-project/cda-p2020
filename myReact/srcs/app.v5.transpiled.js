"use strict";

var _createClass = (function() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    );
  }
  return call && (typeof call === "object" || typeof call === "function")
    ? call
    : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError(
      "Super expression must either be null or a function, not " +
        typeof superClass
    );
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass)
    Object.setPrototypeOf
      ? Object.setPrototypeOf(subClass, superClass)
      : (subClass.__proto__ = superClass);
}

var Counter = (function(_React$Component) {
  _inherits(Counter, _React$Component);

  function Counter(props) {
    _classCallCheck(this, Counter);

    var _this = _possibleConstructorReturn(
      this,
      (Counter.__proto__ || Object.getPrototypeOf(Counter)).call(this, props)
    );

    _this.state = {
      value: 0
    };
    return _this;
  }

  _createClass(Counter, [
    {
      key: "plus",
      value: function plus() {
        this.setState({ value: this.state.value + 1 });
        console.log("+", this.state.value);
      }
    },
    {
      key: "minus",
      value: function minus() {
        this.setState({ value: this.state.value - 1 });
        console.log("-", this.state.value);
      }
    },
    {
      key: "render",
      value: function render() {
        return React.createElement(
          "section",
          { className: "section" },
          React.createElement(
            "div",
            { className: "container" },
            React.createElement(
              "h1",
              { className: "title" },
              " counter-component "
            ),
            React.createElement("hr", null),
            React.createElement(
              "div",
              { className: "content" },
              React.createElement(
                "p",
                null,
                " ",
                "the current value is >",
                React.createElement("strong", null, " ", this.state.value, " "),
                React.createElement("br", null),
                React.createElement("br", null),
                React.createElement(
                  "a",
                  {
                    className: "button is-success",
                    onClick: this.plus.bind(this)
                  },
                  " ",
                  "Plus",
                  " "
                ),
                React.createElement("span", null, " \xA0 "),
                React.createElement(
                  "a",
                  {
                    className: "button is-danger",
                    onClick: this.minus.bind(this)
                  },
                  " ",
                  "Minus",
                  " "
                )
              )
            )
          )
        );
      }
    }
  ]);

  return Counter;
})(React.Component);

ReactDOM.render(
  React.createElement(Counter, null),
  document.getElementById("root")
);
