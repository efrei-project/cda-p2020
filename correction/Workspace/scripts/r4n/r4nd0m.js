//
// Author: Majdi Toumi
// Created: Thu Apr  5 22:17:02 2018 (+0200)
// Last-Updated: Thu Apr  5 22:17:05 2018 (+0200)
//          By: Majdi Toumi
// Version:
//
// THE TEA-WARE LICENSE
// Majdi Toumi wrote this file
// As long you retain this notice, you can do whatever
// you want with this stuff. If we meet some day, and you think
// this stuff is worth it, you can buy me a cup of tea in return.
//
// Let's Rock!
//

const { sample } = require("lodash");
const fs = require("fs");
const colors = require("colors");

const students = require("./data/cda-p2020.json");

process.stdout.write(colors.cyan("Suspense......"));
setTimeout(() => {
  console.log(
    colors.underline.yellow(sample(students)) +
      colors.cyan(" it's time to cook! :)")
  );
}, 2000);
