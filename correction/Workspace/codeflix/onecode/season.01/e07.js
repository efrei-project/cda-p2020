const { existsSync, statSync } = require("fs");

module.exports = file => {
  if (existsSync(file)) {
    const st = statSync(file);
    let type = st.isFile(file)
      ? "file"
      : st.isDirectory(file)
        ? "Directory"
        : "other things";
    console.log(`The argument [ ${file} ] is ${type}`);
  }
};
