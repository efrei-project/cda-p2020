const { unlinkSync } = require("fs");

module.exports = filename => {
  unlinkSync(filename);
  console.log(`${filename} has been removed`);
};
