const { accessSync, constants } = require("fs");

module.exports = file => {
  try {
    accessSync(file, constants.R_OK | constants.W_OK);
    console.log(`I can read and write ${file}`);
  } catch (err) {
    console.log("no access ");
  }
};
