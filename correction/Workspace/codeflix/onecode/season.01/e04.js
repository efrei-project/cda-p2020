const { readFile } = require("fs");

module.exports = (filename, cb) => {
  readFile(filename, "utf8", (err, data) => {
    if (err) throw err;
    cb(data);
  });
};
