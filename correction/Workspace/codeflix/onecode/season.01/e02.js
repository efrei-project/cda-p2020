const path = require("path");
module.exports = file => {
  return path.extname(file);
};
