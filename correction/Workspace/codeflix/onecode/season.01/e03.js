// const path = require("path");
const { readFileSync } = require("fs");
module.exports = filename => {
  return readFileSync(filename, "utf8");
};
