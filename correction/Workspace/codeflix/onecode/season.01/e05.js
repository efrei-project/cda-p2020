const { writeFileSync } = require("fs");

module.exports = (filename, content) => {
  writeFileSync(filename, content, "utf8");
};
