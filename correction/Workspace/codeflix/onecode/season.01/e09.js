const { chmodSync } = require("fs");

module.exports = (file, mode) => {
  chmodSync(file, parseInt(mode, 8));
};
