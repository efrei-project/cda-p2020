#include <stdio.h>
#include <stdlib.h>

/* Arbres binaires base */

//structure maillon
typedef struct tree_node {
  int info;
  struct tree_node * left,*right;
} tree_node;

//AB en tant que pointeur sur un noeud racine
typedef tree_node * tree;

tree creat_node (int b)
{
  tree node;
  node = malloc(sizeof(tree_node));
  node->info = b;
  node->left = NULL;
  node->right = NULL;
  return node;
}


tree creat_perf_tree(int n, int e)
{
  printf("a");
  tree a = creat_node(e);
  if (n>0)
  {
    a->left = creat_perf_tree(n-1, 2*e);
    a->right = creat_perf_tree(n-1, 2*e+1);
  }
  return a;
}



void print_prefix (char c, int n) {while(n--)putchar(c);}

void print_tree (tree t, int p){
  print_prefix ('_', 2*p);
  if (!t) printf ("x\n");
  else{
    printf("%d\n",t->info);
    if (t->left||t->right)
    {
      print_tree(t->left,p+1);
      print_tree(t->right,p+1);
    }
  }
}



int main() {
  int h,r = 0;
  printf("Nous allons creer un arbre\nQuel hauteur voulez-vous pour l'arbre?");
  scanf ("%d",&h);
  printf("Entrez la valeur du noeud racine");
  scanf("%d",&r);
  tree a;
  a = creat_perf_tree(h,r);
  print_tree(a,0);
}
