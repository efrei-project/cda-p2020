import express from 'express'
import bodyParser from 'body-parser'
import passport from 'passport'
import { mLog, argv } from './libs/utils'
import api from './routes/api'
import routes from './routes'
import { db as database } from './models'

import { initializeWorkspace } from './libs/services/storage'

import './middlewares/passport'

const app = express()
const port = parseInt(argv[0], 10) || process.env.PORT

app.use(passport.initialize())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const start = async () => {
  try {
    await database.authenticate()
    mLog('Connected to postgreSQL database', 'green')

    await initializeWorkspace()
    mLog('Workspace created', 'green')

    if (process.env.NODE_ENV === 'development') {
      mLog('Synchronise database')
      database.sync({ force: false })
    }

    app.use('/', routes)
    app.use('/api', api)

    app.use((req, res, next) => {
      const err = new Error('Route not found')
      err.status = 404
      next(err)
    })

    app.use((err, res) => {
      res.status(err.status || 500).json({ err: err.message })
    })

    app.listen(port, (err) => {
      if (err) throw err
      mLog(`Server is running on port ${port}`)
    })
  } catch (err) {
    mLog(err, 'red')
    process.exit(1)
  }
}

start()
