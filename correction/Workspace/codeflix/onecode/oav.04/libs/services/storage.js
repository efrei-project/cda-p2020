import fs from 'fs'
import path from 'path'

const { ROOT_PATH_STORAGE } = process.env

function initializeWorkspace() {
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(ROOT_PATH_STORAGE)) {
      fs.mkdir(ROOT_PATH_STORAGE, (err) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    } else {
      resolve()
    }
  })
}

function addUserWorkspace(user_uuid) {
  const userPath = path.join(ROOT_PATH_STORAGE, user_uuid)

  return new Promise((resolve, reject) => {
    if (!fs.existsSync(userPath)) {
      fs.mkdir(userPath, (err) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    } else {
      resolve()
    }
  })
}

function createBucket(uuid, name) {
  const userPath = path.join(ROOT_PATH_STORAGE, uuid)
  const bucketPath = path.join(userPath, name)

  return new Promise((resolve, reject) => {
    if (!fs.existsSync(bucketPath)) {
      fs.mkdir(bucketPath, (err) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    } else {
      resolve()
    }
  })
}

function renameBucket(uuid, oldName, newName) {
  const userPath = path.join(ROOT_PATH_STORAGE, uuid)
  const oldBucketPath = path.join(userPath, oldName)
  const newBucketPath = path.join(userPath, newName)

  return new Promise((resolve, reject) => {
    fs.rename(oldBucketPath, newBucketPath, (err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

function destroyBucket(uuid, name) {
  const userPath = path.join(ROOT_PATH_STORAGE, uuid)
  const bucketPath = path.join(userPath, name)

  return new Promise((resolve, reject) => {
    fs.rmdir(bucketPath, (err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

function createBlob(uuid, bucketName, name) {}

function renameBlob(uuid, bucketName, oldBlobName, newBlobName) {}

function destroyBlob(uuid, bucketName, name) {}

export {
  initializeWorkspace,
  addUserWorkspace,
  createBucket,
  renameBucket,
  destroyBucket,
  createBlob,
  renameBlob,
  destroyBlob,
}
