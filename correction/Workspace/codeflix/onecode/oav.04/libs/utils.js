import kleur from 'kleur'
import moment from 'moment'

export const argv = process.argv.slice(2)

export function mLog(str, c = 'magenta', withNewline = true) {
  const display = kleur[c](`${moment().format()} - ${str}`)
  if (withNewline) {
    console.log(display)
  } else {
    process.stdout.write(display)
  }
}
