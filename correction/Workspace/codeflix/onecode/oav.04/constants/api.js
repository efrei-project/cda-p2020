export const BAD_REQUEST = {
  status: 400,
  name: 'BAD_REQUEST',
}

export const FORBIDDEN = {
  status: 403,
  name: 'FORBIDDEN',
}
