module.exports = {
  apps: [
    {
      name: 'myS3',
      script: 'server.js',

      instances: 1,
      autorestart: true,
      restart_delay: '5000',
      watch: true,

      max_memory_restart: '1G',
      interpreter: 'babel-node',

      env: {
        PORT: 5000,
        DATABASE_URL: 'postgres://Student:@localhost:5432/myS3.dev',
        NODE_ENV: 'development',
        JWT_ENCRYPTION: 'pomme',

        ROOT_PATH_STORAGE: '/Users/test/myS3',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],
}
