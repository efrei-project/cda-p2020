import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as JsonWebTokenStrategy, ExtractJwt } from 'passport-jwt'
import User from '../models/user'

passport.use(
  new LocalStrategy(
    {
      usernameField: 'nickname',
      passwordField: 'password',
    },
    async (nickname, password, done) => {
      try {
        const user = await User.findOne({ where: { nickname } })
        if (!user) {
          return done(new Error('Incorrect nickname'))
        }
        if (!(await user.checkPassword(password))) {
          return done(new Error('Incorrect password'))
        }
        return done(false, user)
      } catch (err) {
        return done(new Error(`Something wrong: ${err}`))
      }
    },
  ),
)

passport.use(
  new JsonWebTokenStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_ENCRYPTION,
    },
    async (jwtPayload, done) => {
      try {
        const user = await User.findByPk(jwtPayload.uuid)

        if (user) {
          return done(null, user)
        }
        return done('User does not exist')
      } catch (err) {
        return done(`Something wrong: ${err}`)
      }
    },
  ),
)
