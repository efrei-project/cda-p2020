import { Router } from 'express'
import passport from 'passport'
import User from '../../models/user'
import { addUserWorkspace } from '../../libs/services/storage'
import { BAD_REQUEST } from '../../constants/api'
import { success, error } from '../../helpers/response'

const api = Router()

api.post('/register', async (req, res) => {
  const {
    nickname, email, password, password_confirmation,
  } = req.body

  try {
    const user = new User({
      nickname,
      email,
      password,
      password_confirmation,
    })

    await user.save()

    await addUserWorkspace(user.uuid)

    const token = user.generateJwtToken()

    res.json(success({ user }, { token }))
  } catch (err) {
    res.status(400).json(error(BAD_REQUEST, err))
  }
})

api.post('/login', (req, res, next) => {
  passport.authenticate('local', { session: false }, (err, user) => {
    if (err) {
      return res.status(400).json(error(BAD_REQUEST, err))
    }

    const token = user.generateJwtToken()

    return res.json(success({ user }, { token }))
  })(req, res, next)
})

export default api
