import { Router } from 'express'
import { pick } from 'lodash'
import { success } from '../../../helpers/response'

const api = Router()

api.get('/:uuid', async (req, res) => {
  try {
    const { user } = req
    res.json(success({ user }))
  } catch (err) {
    res.status(400).json({ err })
  }
})

api.put('/:uuid', async (req, res) => {
  try {
    const { user } = req
    const { uuid } = req.params

    if (user.uuid !== uuid) {
      res.status(403).json({ err: 'Permission denied' })
    }

    const fields = pick(req.body, ['nickname', 'email', 'password', 'password_confirmation'])
    await user.update(fields)

    res.status(204).end()
  } catch (err) {
    res.status(400).json({ err: err.message })
  }
})

export default api
