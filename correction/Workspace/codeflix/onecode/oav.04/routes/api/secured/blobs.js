import { Router } from 'express'
import multer from 'multer'
import path from 'path'
import Bucket from '../../../models/bucket'
import Blob from '../../../models/blob'
import { success } from '../../../helpers/response'

import { renameBlob, destroyBlob } from '../../../libs/services/storage'

const api = Router()

const storage = multer.diskStorage({
  async destination(req, file, cb) {
    const { bucket_id } = req.params
    const { ROOT_PATH_STORAGE } = process.env

    const bucket = await Bucket.findByPk(bucket_id)

    let dirname

    if (bucket) {
      dirname = path.join(ROOT_PATH_STORAGE, req.user.uuid, bucket.name)
    } else {
      dirname = path.join('/', 'tmp')
    }

    cb(null, dirname)
  },
  filename(req, file, cb) {
    const { name } = req.body

    if (!name) {
      throw new Error('Field name must be define')
    }

    const uploadedName = name + path.extname(file.originalname)
    cb(null, uploadedName)
  },
})

const upload = multer({ storage })

api.post('/', upload.single('file_to_upload'), async (req, res) => {
  try {
    const { name } = req.body
    const { size, path: p } = req.file

    const blob = new Blob({ name, size, path: p })

    await blob.save()

    res.status(201).end()
  } catch (err) {
    res.status(400).json({ err: err.message })
  }
})

api.post('/copy', async (req, res) => {
  try {
  } catch (err) {
    res.status(400).json({ err: err.message })
  }
})

api.get('/:id', async (req, res) => {
  try {
    const { id } = req.params

    const blob = await Blob.findByPk(id)

    if (blob) {
      res.status(200).json(success({ blob }))
    } else {
      res.status(400).json({ data: { blob } })
    }
  } catch (err) {
    res.status(400).json({ err })
  }
})

api.put('/:id', async (req, res) => {
  try {
  } catch (err) {
    res.status(400).json({ err: err.message })
  }
})

api.delete('/:id', async (req, res) => {
  try {
    const { uuid } = req.user
    const { bucket_id } = req.params
    const { id } = req.params

    const bucket = await Bucket.findByPk(bucket_id)

    const blob = await Blob.findByPk(id)
    await blob.destroy()

    await destroyBlob(uuid, bucket.name, blob.name)

    res.status(204).end()
  } catch (err) {
    res.status(400).json({ err: err.message })
  }
})

export default api
