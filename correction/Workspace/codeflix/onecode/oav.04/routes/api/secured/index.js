import { Router } from 'express'
import users from './users'
import buckets from './buckets'
import blobs from './blobs'

const api = Router()

api.use('/users', users)
api.use('/users/:uuid/buckets', buckets)
api.use('/users/:uuid/buckets/:bucket_id/blobs', blobs)

export default api
