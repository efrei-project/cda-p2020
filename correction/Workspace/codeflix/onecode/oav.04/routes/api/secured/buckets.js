import { Router } from 'express'
import Bucket from '../../../models/bucket'
import { createBucket, renameBucket, destroyBucket } from '../../../libs/services/storage'

const api = Router()

api.post('/', async (req, res) => {
  try {
    const { uuid: user_uuid } = req.user
    const { name } = req.body

    const existing_bucket = await Bucket.findOne({ where: { name } })
    if (existing_bucket) {
      throw new Error(`Bucket ${name} already exist!`)
    }

    const bucket = new Bucket({ name, user_uuid })
    await bucket.save()

    await createBucket(user_uuid, bucket.name)

    res.status(201).json({ data: { bucket } })
  } catch (err) {
    res.status(400).json({ err: err.message })
  }
})

api.get('/', async (req, res) => {
  try {
    const { uuid: user_uuid } = req.user
    const buckets = await Bucket.findAll({ where: { user_uuid } })
    res.status(200).json({ data: { buckets } })
  } catch (err) {
    res.status(400).json({ err })
  }
})

api.head('/:id', async (req, res) => {
  try {
    const { id } = req.params
    const bucket = await Bucket.findByPk(id)
    res.status(bucket ? 200 : 400).end()
  } catch (err) {
    res.status(400).json({ err })
  }
})

api.put('/:id', async (req, res) => {
  try {
    const { uuid } = req.user
    const { id } = req.params
    const { name } = req.body

    const bucket = await Bucket.findByPk(id)

    const oldName = bucket.name
    await bucket.update({ name })

    await renameBucket(uuid, oldName, bucket.name)

    res.status(204).end()
  } catch (err) {
    res.status(400).json({ err: err.message })
  }
})

api.delete('/:id', async (req, res) => {
  try {
    const { uuid } = req.user
    const { id } = req.params

    const bucket = await Bucket.findByPk(id)
    await bucket.destroy()

    await destroyBucket(uuid, bucket.name)

    res.status(204).end()
  } catch (err) {
    res.status(400).json({ err: err.message })
  }
})

export default api
