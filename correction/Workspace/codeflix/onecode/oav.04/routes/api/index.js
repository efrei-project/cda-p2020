import { Router } from 'express'
import passport from 'passport'
import auth from './auth'
import secured from './secured'

const api = Router()

api.get('/', (req, res) => {
  res.status(200).json({
    api: 'myS3Api',
    meta: {
      version: '0.0.1',
      status: 'running',
    },
  })
})

api.use('/auth', auth)
api.use('/', passport.authenticate('jwt', { session: false }), secured)

export default api
