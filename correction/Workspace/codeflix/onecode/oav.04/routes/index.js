import { Router } from 'express'

const router = Router()

router.get('/', (req, res) => {
  res.status(200).json({
    hi: 'Use our api with the endpoint: /api',
  })
})

export default router
