import { Model } from 'sequelize'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

import { USER_MIN_NICKNAME_LENGTH, USER_MIN_PASSWORD_LENGTH } from '../constants/validation'

export default class User extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        uuid: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
        },
        nickname: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            isLongEnough(v) {
              if (v.length < USER_MIN_NICKNAME_LENGTH) {
                throw new Error('Please choose a longer nickname')
              }
            },
          },
          unique: {
            args: true,
            msg: 'nickname already in use',
          },
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            isEmail: true,
          },
          unique: {
            args: true,
            msg: 'email already in use',
          },
        },
        password: {
          type: DataTypes.VIRTUAL,
          allowNull: false,
          validate: {
            isLongEnough(v) {
              if (v.length < USER_MIN_PASSWORD_LENGTH) {
                throw new Error('Please choose a longer password')
              }
            },
          },
        },
        password_confirmation: {
          type: DataTypes.VIRTUAL,
          allowNull: false,
          validate: {
            isEqual(v) {
              if (v !== this.password) {
                throw new Error('Password confirmation doesn\'t match')
              }
            },
          },
        },
        password_digest: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notEmpty: true,
          },
        },
      },
      {
        sequelize,
        tableName: 'users',
        indexes: [
          {
            fields: ['uuid', 'email'],
          },
        ],
        hooks: {
          async beforeValidate(userInstance) {
            if (userInstance.isNewRecord) {
              userInstance.password_digest = await userInstance.createHash(userInstance.password)
            }
          },
        },
        async beforeSave(userInstance) {
          if (userInstance.changed('password')) {
            if (userInstance.password !== userInstance.password_confirmation) {
              throw new Error('Password confirmation doesn\'t match')
            }
            userInstance.password_digest = await userInstance.createHash(userInstance.password)
          }
        },
      },
    )
  }

  async createHash(password) {
    const SALT_ROUNDS = 10
    const hash = await bcrypt.hash(password, SALT_ROUNDS)
    if (!hash) {
      throw new Error('Can\'t hash password')
    }
    return hash
  }

  async checkPassword(password) {
    return bcrypt.compare(password, this.password_digest)
  }

  toJSON() {
    const fields = this.get()
    delete fields.password_digest

    return fields
  }

  generateJwtToken() {
    const payload = { uuid: this.uuid, nickname: this.nickname, email: this.email }
    return jwt.sign(payload, process.env.JWT_ENCRYPTION)
  }

  static associate(models) {
    this.hasMany(models.Bucket, {
      as: 'buckets',
      onDelete: 'cascade',
    })
  }
}
