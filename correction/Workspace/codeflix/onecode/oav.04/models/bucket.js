import { Model } from 'sequelize'

export default class Bucket extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        tableName: 'buckets',
        indexes: [
          {
            fields: ['id'],
          },
        ],
      },
    )
  }

  static associate(models) {
    this.belongsTo(models.User, { as: 'user' })
    this.hasMany(models.Blob, {
      as: 'blobs',
      onDelete: 'cascade',
    })
  }
}
