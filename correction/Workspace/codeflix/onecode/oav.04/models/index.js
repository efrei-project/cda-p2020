import Sequelize, { Op } from 'sequelize'
import User from './user'
import Bucket from './bucket'
import Blob from './blob'

export const db = new Sequelize(process.env.DATABASE_URL, {
  operatorsAliases: Op,
  define: {
    underscored: true,
  },
})

export const models = {
  User: User.init(db, Sequelize),
  Bucket: Bucket.init(db, Sequelize),
  Blob: Blob.init(db, Sequelize),
}

Object.values(models)
  .filter(model => model.associate)
  .forEach(model => model.associate(models))
