export function success(resource, meta) {
  return { data: resource, meta }
}

export function error({ status, name }, { message }) {
  return {
    err: {
      status,
      name,
      message,
    },
  }
}
