import express from 'express'
import socketIo from 'socket.io'
import { createServer } from 'http'

const port = parseInt(process.argv[2], 10) || process.env.PORT

const app = express()

const server = createServer(app)

const io = socketIo(server)

io.on('connection', (socket) => {
  console.log('Client connected!')
  socket.on('disconnect', () => {
    console.log('Client disconnected!')
  })
})

app.listen(port, (err) => {
  if (err) throw err
  console.log(`Server running on ${port}.`)
})
