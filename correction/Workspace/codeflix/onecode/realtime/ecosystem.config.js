module.exports = {
  apps: [
    {
      name: 'ws',
      script: 'server.js',

      instances: 1,
      autorestart: true,
      restart_delay: '5000',
      watch: true,

      max_memory_restart: '1G',
      interpreter: 'babel-node',

      env: {
        PORT: 5000,
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],
}
