const http = require("http");
const https = require("https");
const url = require("url");
const fs = require("fs");

const REDDIT_JSON = "https://www.reddit.com/r/perfectloops.json";
const MIN_TIME_REQUEST = 60000;

const port = parseInt(process.argv[2], 10) || 4242;

let lastRequest;
let gifs = {};

function throttle(func, wait = 0) {
  const now = new Date().getTime();

  if (!lastRequest || now > lastRequest + wait) {
    lastRequest = now;
    return func;
  } else {
    return cb => cb(gifs);
  }
}

function fetchFromReddit(cb) {
  https.get(REDDIT_JSON, response => {
    let data = "";

    response.on("data", chunk => {
      data += chunk;
    });

    response.on("end", chunk => {
      const json = JSON.parse(data);
      gifs = json.data.children.slice(-10);
      cb(gifs);
    });
  });
}

http
  .createServer((req, res) => {
    res.setHeader("Content-Type", "application/json");

    const throttled = throttle(fetchFromReddit, MIN_TIME_REQUEST);

    throttled(data => {
      res.write(JSON.stringify(data));
      res.end();
    });
  })
  .listen(port, () => {
    console.log(`Server is listening on port ${port}`);
  });
