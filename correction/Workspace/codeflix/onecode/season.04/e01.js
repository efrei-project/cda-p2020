const http = require("http");
const url = require("url");

const port = parseInt(process.argv[2], 10) || 4242;
const magicNumber = Math.floor(Math.random() * 1337) + 1;

let counter = 1;

http
  .createServer((req, res) => {
    const { query } = url.parse(req.url, true);
    if (magicNumber === parseInt(query.n, 10)) {
      res.write(`Congrats! ${counter} shot`);
    } else {
      res.write(query.n < magicNumber ? "Bigger!" : "Smaller");
      counter++;
    }

    res.end();
  })
  .listen(port, () => {
    console.log(`Server is listening on port ${port}`);
  });
