const http = require("http");
const url = require("url");
const fs = require("fs");

const MESSSAGE_PATH = "messages.json";
const port = parseInt(process.argv[2], 10) || 4242;

if (!fs.existsSync(MESSSAGE_PATH)) {
  const data = {
    messages: []
  };

  fs.writeFileSync(MESSSAGE_PATH, JSON.stringify(data, null, 2));
}

http
  .createServer((req, res) => {
    res.setHeader("Content-Type", "application/json");

    const reqURL = url.parse(req.url, true).path;
    if (reqURL === "/messages") {
      if (req.method === "GET") {
        res.write(fs.readFileSync(MESSSAGE_PATH));
        res.end();
      } else if (req.method === "POST") {
        let body = "";
        req.on("data", chunk => {
          body += chunk;
        });

        req.on("end", chunk => {
          const data = JSON.parse(fs.readFileSync(MESSSAGE_PATH));
          data.messages.push({
            text: body,
            date: new Date().toISOString()
          });

          fs.writeFileSync(MESSSAGE_PATH, JSON.stringify(data, null, 2));

          res.end();
        });
      }
    } else {
      res.end();
    }
  })
  .listen(port, () => {
    console.log(`Server is listening on port ${port}`);
  });
