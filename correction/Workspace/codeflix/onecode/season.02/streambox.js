const fs = require("fs");
const { Transform } = require("stream");

function duplicate(fileName) {
  if (fs.existsSync(fileName)) {
    let rStream = fs.createReadStream(fileName);
    let wStream = fs.createWriteStream(`${fileName}.duplicate`);
    rStream.pipe(wStream);
  }
}

function transform(fileName, regex, func, in_stdout = true) {
  let rStream = fs.createReadStream(fileName);

  if (in_stdout) {
    let content = "";
    rStream.on("data", chunk => {
      content += chunk;
    });
    rStream.on("end", () => {
      content = content.replace(regex, func);
      console.log(content);
    });
  } else {
    let tStream = new Transform({
      transform(chunk, encoding, callback) {
        this.push(chunk.toString().replace(regex, func));
        callback();
      }
    });
    let wStream = fs.createWriteStream(`${fileName}.transform`);
    rStream.pipe(tStream).pipe(wStream);
  }
}

module.exports = {
  duplicate: duplicate,
  transform: transform
};
