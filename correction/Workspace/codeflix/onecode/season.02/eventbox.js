const { EventEmitter } = require("events");

function empty() {
  const evnt = new EventEmitter();
  evnt.on("hi", () => {
    console.log("Hello world #1");
  });
  evnt.on("hi", () => {
    console.log("Hello world #2");
  });
  evnt.emit("hi");
}

function withArgs(names) {
  const evnt = new EventEmitter();
  evnt.on("newFellow", fellow => {
    console.log(`newFellow : ${fellow}`);
  });
  for (const n of names) {
    evnt.emit("newFellow", n);
  }
}

module.exports = { empty: empty, withArgs: withArgs };
