const { duplicate, transform } = require("./streambox.js");
const fileName = process.argv[2];

function upper(str) {
  return str.toUpperCase();
}

transform(fileName, /salut/gi, upper, false);
